## Todo

* Make people choose if coin input should have one or three input fields for values (DONE)
    - Put cookie on change, so it loads the same config next time (DONE)
    - Remember input field position, even though number is updated by databinding
* Scroll up and down on number fields should increase / decrease value
* Break even prices
* Search live items and load in it's buyprice
    - Also load in current sell prices
* Move listing fee calculations into own elements
* Make `listingFee()` and `sellingFee()` the same function
